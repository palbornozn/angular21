import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Pedidos } from 'src/app/models/Pedidos';

@Injectable({
  providedIn: 'root'
})
export class CocinaService {

  private URL:string = 'http://localhost:3000/'

  constructor(private http:HttpClient) { }


  getPedidos():Observable<Pedidos[]>{
     return this.http.get<Pedidos[]>(this.URL+'pedidos')
  }

  getPlatos(pedidoid:number){
    return this.http.get(this.URL+'pedidoplatos/'+pedidoid)
  }

  deletePlato(pedidoid:number){
    return this.http.delete(this.URL+'pedidos/'+pedidoid.toString())
  }

  tomarPedido(pedidoid:number,pedido:Pedidos){
    
    return this.http.put(this.URL+'tomarpedido/'+pedidoid.toString(),pedido)
  }

}
