import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  private URL:string = 'http://localhost:3000/'
  constructor(private http: HttpClient) { }

  getData():Observable<any>{
    return this.http.get(this.URL+'dashboard')
  }

}
