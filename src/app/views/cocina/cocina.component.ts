import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription,map,timer } from 'rxjs';
import { ESTADOS } from 'src/app/models/Estados';
import { Pedidos } from 'src/app/models/Pedidos';
import { Platos } from 'src/app/models/Platos';
import { CocinaService } from 'src/app/services/cocina/cocina.service';


@Component({
  selector: 'app-cocina',
  templateUrl: './cocina.component.html',
  styleUrls: ['./cocina.component.css']
})
export class CocinaComponent implements OnInit,OnDestroy {

  estados = ESTADOS
  pedidoList:Pedidos [] = []
  platosForPedido:Platos[] = []

  constructor( private cocinaService:CocinaService) { }

  

  ngOnInit(): void {
   this.getPedidos()
    
  }

  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }
 }
  interval = setInterval(()=>{
    this.getPedidos()
  },15000)

  getPedidos(){
    this.cocinaService.getPedidos().subscribe(data=>{
      this.pedidoList = <any>data 
      console.log(this.pedidoList)
    })

  }
  

  getPlatosForPedido(pedidoid:number){
  
    this.cocinaService.getPlatos(pedidoid).subscribe(data=>{
      this.platosForPedido = <any>data
      console.log(this.platosForPedido)

    })
  }

  deletePedido(pedidoid:number){
    this.cocinaService.deletePlato(pedidoid).subscribe(data=>{
      console.log(data)
      this.getPedidos()
    })
  }

  entregarPedido(pedidoid:number,pedido:any){
      this.cocinaService.tomarPedido(pedidoid,pedido).subscribe(data=>{
        this.getPedidos()
      })
  }
}
