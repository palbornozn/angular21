import { Component, OnInit,OnDestroy } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, MinLengthValidator, ValidatorFn, Validators } from '@angular/forms';
import { ignoreElements, map, Subscription, timer } from 'rxjs';
import { Users } from 'src/app/models/usuario';
import { UserService } from 'src/app/services/user/user.service';


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit, OnDestroy {

  userList: Users[] = [];
  userToEdit: Users = new Users("", "", "", "", "", true, "", "");
  userType:any [] = []

  public userForm = new FormGroup({
    nombre: new FormControl('', Validators.required),
    apellidos: new FormControl('', Validators.required),
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    isActive: new FormControl('', Validators.required),
    tipoUsuarioId: new FormControl('', Validators.required)
  })

  newUserForm = new FormGroup({
    nombre: new FormControl('', Validators.required),
    apellidos: new FormControl('', Validators.required),
    username: new FormControl('', [Validators.required, Validators.minLength(4),]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
    isActive: new FormControl('', Validators.required),
    tipoUsuarioId: new FormControl('', Validators.required)
  })

  constructor(private user: UserService) { }

  

  ngOnInit(): void {
   this.getUsers()
    
    this.user.getUserType().subscribe(data=>{
      this.userType = <any>data
    })
  }
  
  ngOnDestroy():void{
    if(this.interval){
      clearInterval(this.interval)
    }
  }

  interval = setInterval(()=>{
    this.getUsers()
  },15000)

  getUsers() {
    let nodata: any = []
    if (localStorage.getItem('token')) {
      this.user.getUsers().subscribe(data => {
        this.userList = <any>data
      })
    }else {
      return nodata
    }
  }

  deleteUser(user: any) {
    this.user.deleteUser(user).subscribe(data => {
      console.log(data)
      this.getUsers()
    })
  }

  placeHolderUser(user: Users) {
    this.userToEdit = user;
  }

  editUser(userForm: any, usuarioid: any) {
    this.user.editUser(userForm, usuarioid).subscribe(data => {
      this.getUsers()
      alert("Usuario Editado")
    })
  }

  newUser(newUserForm: any) {
    this.user.newUser(newUserForm).subscribe(data => {
      this.getUsers()
      alert("Usuario agregado")
    })
  }

  
}
