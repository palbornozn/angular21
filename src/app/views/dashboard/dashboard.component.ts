import { Component, OnInit } from '@angular/core';
import { DataResumen } from 'src/app/models/Pedidos';
import { DashboardService } from 'src/app/services/dashbaord/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private _dashService:DashboardService) { }

   cardInfo:DataResumen [] = []

  ngOnInit(): void {
    this.getData()
    
  }

  async getData(){
    await this._dashService.getData().subscribe(data=>{
      this.cardInfo = <any>data
      console.log(this.cardInfo)
    })
  }


}
