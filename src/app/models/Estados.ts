export class Estados {

    constructor(
        public estadoId:string,
        public nombre:string,

    ){}


}

export const ESTADOS:Estados[] =[
    {estadoId:'0',nombre:''},
    {estadoId:'1',nombre:'Disponible'},
    {estadoId:'2',nombre:'Ocupada'},
    {estadoId:'3',nombre:'Mantenimiento'},
    {estadoId:'4',nombre:'Preparado'},
    {estadoId:'5',nombre:'Listo'},
    {estadoId:'6',nombre:'Entregado'},
    {estadoId:'7',nombre:'Ordenado'}
]