export class Pedidos {

    constructor(
        public pedidoId:number,
        public mesaId:string,
        public inicioPedido:string,
        public estadoId:number,
        public isActive:boolean
    ){}

}

export class DataResumen {
    constructor(
        public total:string,
        public cantidadPedidos:string,
        public TiempoMaximo:string
    ){ }
}