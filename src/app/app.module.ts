import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { appRoutingProviders, routing } from  './app.routing';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './views/login/login.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CocinaComponent } from './views/cocina/cocina.component';
import { NavbarComponent } from './Shared/navbar/navbar.component';
import { UsuariosComponent } from './views/mantenedores/usuarios/usuarios.component';
import { ProductosComponent } from './views/mantenedores/productos/productos.component';
import { ProveedoresComponent } from './views/mantenedores/proveedores/proveedores.component';
import { MesasComponent } from './views/mantenedores/mesas/mesas.component';
import { OfertasComponent } from './views/mantenedores/ofertas/ofertas.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    CocinaComponent,
    NavbarComponent,
    UsuariosComponent,
    ProductosComponent,
    ProveedoresComponent,
    MesasComponent,
    OfertasComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    routing,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [appRoutingProviders,
  JwtHelperService,
  {provide:JWT_OPTIONS,useValue:JWT_OPTIONS}],
  bootstrap: [AppComponent]
})
export class AppModule { }
